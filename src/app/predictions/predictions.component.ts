import { Component, OnDestroy } from '@angular/core';
import { Prediction } from '../prediction';
import { Store } from '@ngrx/store';
import * as appState from '../reducers.index';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
    selector: 'predictions',
    templateUrl: './predictions.component.html',
    styleUrls: ['./predictions.component.scss']
})
export class PredictionsComponent implements OnDestroy {

    public predictions: Prediction[] = [];

    constructor(private _store: Store<appState.State>) {
        this._store.select(appState.getPredictions).pipe(untilDestroyed(this)).subscribe(predictions => {
            this.predictions = predictions;
        });
    }

    ngOnDestroy(): void { }
}