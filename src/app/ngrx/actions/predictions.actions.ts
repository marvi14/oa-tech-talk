import { Action } from '@ngrx/store';
import { Prediction } from '../../prediction';

export const ActionTypes = {
    NOTIFY_PREDICTIONS: 'Notify Predictions to Stakeholders',
    RESET_PREDICTIONS: 'Reset Predictions'
};

export class NotifyPredictionsAction implements Action {
    type = ActionTypes.NOTIFY_PREDICTIONS;

    constructor(public payload: Prediction[]) { }
}

export class ResetPredictionsAction implements Action {
    type = ActionTypes.RESET_PREDICTIONS;

    constructor(public payload?: any) { }
}

export type Actions = NotifyPredictionsAction | ResetPredictionsAction