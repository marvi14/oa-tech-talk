import { Prediction } from '../../prediction';
import * as predictionsActions from '../actions/predictions.actions';

export interface PredictionsState {
    predictions: Prediction[],
    explicitContent: boolean
};

const initialState: PredictionsState = { predictions: [], explicitContent: false };

export function reducer(state = initialState, action: predictionsActions.Actions): PredictionsState {

    switch (action.type) {

        case predictionsActions.ActionTypes.NOTIFY_PREDICTIONS: {
            return {
                predictions: action.payload,
                explicitContent: (action.payload as Prediction[]).some(prediction => (prediction.className === 'Sexy' || prediction.className === 'Porn') && prediction.probability > 0.5)
            };
        }

        case predictionsActions.ActionTypes.RESET_PREDICTIONS: {
            return {
                predictions: [],
                explicitContent: false
            };
        }

        default:
            return state;
    }
};