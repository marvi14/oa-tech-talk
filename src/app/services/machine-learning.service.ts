import { Injectable } from '@angular/core';
import * as mobilenet from '@tensorflow-models/mobilenet';
import * as nsfwjs from 'nsfwjs';
import { Observable, forkJoin, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Prediction } from '../prediction';
import { orderBy } from 'lodash';

@Injectable()
export class MachineLearningService {

    private model: mobilenet.MobileNet;
    private nsfwjsModel: any;

    public loadModels(): Observable<any> {
        return from(new Promise((resolve) => {
            mobilenet.load().then(model => {
                this.model = model;
                nsfwjs.load().then(otherModel => {
                    this.nsfwjsModel = otherModel;
                    resolve(true);
                });
            });
        }));
    }

    public predict(element): Observable<Prediction[]> {
        return forkJoin(this.nsfwjsModel.classify(element), this.model.classify(element)).pipe(map(predictions => {
            const mobilenet = predictions[0] as Prediction[];
            const nsfwjs = predictions[1] as Prediction[];
            mobilenet.push(...nsfwjs);
            return orderBy(mobilenet, 'probability', 'desc').filter(prediction => prediction.probability > 0.1);
        }));
    }
}