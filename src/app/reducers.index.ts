import { ActionReducerMap, createSelector } from '@ngrx/store';
import * as predictionsStore from './ngrx/reducers/predictions.reducer';
import { PredictionsState } from './ngrx/reducers/predictions.reducer';

export interface State {
    predictionsState: predictionsStore.PredictionsState;
}

export const reducers: ActionReducerMap<State> = {
    predictionsState: predictionsStore.reducer
}

export const getPredictions = createSelector((state: State) => state.predictionsState, (state: PredictionsState) => state.predictions);
export const getExplicitContent = createSelector((state: State) => state.predictionsState, (state: PredictionsState) => state.explicitContent);