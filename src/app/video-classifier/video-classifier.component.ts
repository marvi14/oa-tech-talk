import { Component, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MachineLearningService } from '../services/machine-learning.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Store } from '@ngrx/store';
import * as appState from '../reducers.index';
import * as actions from '../ngrx/actions/predictions.actions';

@Component({
    selector: 'video-classifier',
    templateUrl: './video-classifier.component.html',
    styleUrls: ['./video-classifier.component.scss']
})
export class VideoClassifierComponent implements OnDestroy {

    @ViewChild('video', { static: false }) video: ElementRef;
    private camInterval;

    constructor(private mlService: MachineLearningService, private _store: Store<appState.State>) { }

    ngAfterViewInit() {
        const vid = this.video.nativeElement;
        if (navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true }).then((stream) => {
                vid.srcObject = stream;
            }).catch((err0r) => {
                console.log('Something went wrong!');
            });
            this.camInterval = setInterval(() => {
                this.mlService.predict(vid).pipe(untilDestroyed(this)).subscribe(predictions => {
                    this._store.dispatch(new actions.NotifyPredictionsAction(predictions));
                });
            }, 1000);
        }
    }

    ngOnDestroy(): void {
        clearInterval(this.camInterval);
    }
}