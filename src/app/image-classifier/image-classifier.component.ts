import { Component, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { MachineLearningService } from '../services/machine-learning.service';
import { Store } from '@ngrx/store';
import * as appState from '../reducers.index';
import * as actions from '../ngrx/actions/predictions.actions';
import { untilDestroyed } from 'ngx-take-until-destroy';
declare var Swal: any;

@Component({
    selector: 'image-classifier',
    templateUrl: './image-classifier.component.html',
    styleUrls: ['./image-classifier.component.scss']
})
export class ImageClassifierComponent {

    public imageSrc: string;
    @ViewChild('img', { static: false }) imageEl: ElementRef;
    public explicitContent = false;

    constructor(private changeDetectorRef: ChangeDetectorRef, private mlService: MachineLearningService, private _store: Store<appState.State>) {
        this._store.select(appState.getExplicitContent).pipe(untilDestroyed(this)).subscribe(explicitContent => {
            this.explicitContent = explicitContent;
            if (this.explicitContent) {
                Swal.fire({
                    icon: 'error',
                    title: 'Ups...',
                    text: 'Puede haber cosas feas'
                });
            }
        });
    }

    public fileChange(event) {
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (res: any) => {
                this.imageSrc = res.target.result;
                this.changeDetectorRef.detectChanges();
                setTimeout(() => {
                    this.mlService.predict(this.imageEl.nativeElement).pipe(untilDestroyed(this)).subscribe(predictions => {
                        this._store.dispatch(new actions.NotifyPredictionsAction(predictions));
                    });
                });
            };
        }
    }

    ngOnDestroy(): void { }
}