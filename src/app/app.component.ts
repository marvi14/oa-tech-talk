import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { MachineLearningService } from './services/machine-learning.service';
import { take } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as appState from './reducers.index';
import * as actions from './ngrx/actions/predictions.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public explicitContent = false;
  public showingCam = false;

  constructor(private spinner: NgxSpinnerService, private mlService: MachineLearningService, private _store: Store<appState.State>) { }

  ngOnInit(): void {
    this.spinner.show();
    this.mlService.loadModels().pipe(take(1)).subscribe(() => {
      this.spinner.hide();
    });
  }

  public toggleSource() {
    this.showingCam = !this.showingCam;
    this._store.dispatch(new actions.ResetPredictionsAction());
  }
}
