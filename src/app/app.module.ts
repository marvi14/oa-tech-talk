import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { MachineLearningService } from './services/machine-learning.service';
import { ImageClassifierComponent } from './image-classifier/image-classifier.component';
import { PredictionsComponent } from './predictions/predictions.component';
import { VideoClassifierComponent } from './video-classifier/video-classifier.component';
import { StoreModule } from "@ngrx/store";
import { reducers } from './reducers.index';

@NgModule({
  declarations: [
    AppComponent,
    VideoClassifierComponent,
    ImageClassifierComponent,
    PredictionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    StoreModule.forRoot(reducers)
  ],
  providers: [MachineLearningService],
  bootstrap: [AppComponent]
})
export class AppModule { }
